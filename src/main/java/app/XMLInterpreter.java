package app;

import org.jdom2.Content;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.filter.Filters;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;

import javax.swing.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class XMLInterpreter {
    Document document = null;

    public Document getDocument() { return document; }
    public void setDocument(Document document) { this.document = document; }

    public XMLInterpreter(String xmlName){

        //Seteo documento y le paso el nombre del doc
        setXML(xmlName);

        //creo la clase que va a diseñar el panel
        PanelBuilder miPanelBuilder = new PanelBuilder();

        //funciones que parsear los botones y texfields
        parseButtons(miPanelBuilder);
        parseTextFields(miPanelBuilder);

        //crear el frame con todos los datos ya parseados
        miPanelBuilder.createFrame();

    }

    private void parseButtons(PanelBuilder panel){
        //Parseo todos los botones para lograr obtener la cantidad de botones
        XPathFactory xpfac = XPathFactory.instance();
        XPathExpression<Content> exprButtons = xpfac.compile("/frame/panel/buttons/button", Filters.content());
        List<Content> listLinksButtons = exprButtons.evaluate(this.document);
        int sizeLinksButtons = listLinksButtons.size();

        //Sabiendo de antemano cuales son los nombres me creo listas de esos datos.
        //Es decir: Todos los datos en la primera posición de la lista, equivale al boton[1]
        //los datos en la segunda posición son del boton[2]
        //Y así sucesivamente cuantos botones tenga

        //[name1,name2]
        List<String> listNameButtons = parserXML("buttons","button",sizeLinksButtons,"name");
        //[bounds-x1,bounds-x2]
        List<String> listXButtons = parserXML("buttons","button",sizeLinksButtons,"b-bounds-x");
        List<String> listYButtons = parserXML("buttons","button",sizeLinksButtons,"b-bounds-y");
        List<String> listWButtons = parserXML("buttons","button",sizeLinksButtons,"b-bounds-w");
        List<String> listHButtons = parserXML("buttons","button",sizeLinksButtons,"b-bounds-h");

        //Le paso al creador de paneles las listas creadas
        panel.setSizeButtons(sizeLinksButtons);
        panel.setListNameButtons(listNameButtons);
        panel.setListXButtons(listXButtons);
        panel.setListYButtons(listYButtons);
        panel.setListWButtons(listWButtons);
        panel.setListHButtons(listHButtons);
    }

    private void parseTextFields(PanelBuilder panel){
        //Parseo todos los botones para lograr obtener la cantidad de botones
        XPathFactory xpfac = XPathFactory.instance();
        XPathExpression<Content> exprTextField = xpfac.compile("/frame/panel/textFields/textField", Filters.content());
        List<Content> listLinksTextFields = exprTextField.evaluate(this.document);
        int sizeLinksTextFields = listLinksTextFields.size();

        List<String> listXTextFields = parserXML("textFields","textField",sizeLinksTextFields,"t-bounds-x");
        List<String> listYTextFields = parserXML("textFields","textField",sizeLinksTextFields,"t-bounds-y");
        List<String> listWTextFields = parserXML("textFields","textField",sizeLinksTextFields,"t-bounds-w");
        List<String> listHTextFields = parserXML("textFields","textField",sizeLinksTextFields,"t-bounds-h");

        //Le paso al creador de paneles las listas creadas
        panel.setSizeTextFields(sizeLinksTextFields);
        panel.setListXTextFields(listXTextFields);
        panel.setListYTextFields(listYTextFields);
        panel.setListWTextFields(listWTextFields);
        panel.setListHTextFields(listHTextFields);
    }

    public void setXML(String fileName){
        SAXBuilder sax = new SAXBuilder();
        try {
            setDocument(sax.build("/home/matias/workspace-intellij/remoteXML/" + fileName + ".xml"));
        }
        catch (JDOMException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Bien genérico para Buttons o TextFields o demas objetos de panels
    public List<String> parserXML(String selectorHijo, String selectorHijo2, int size, String lastSon){
        List<String> listaResultados = new ArrayList<String>();
        for(int x=1; x<=size; x++){
            String querie = "/frame/panel/"+ selectorHijo +"/" + selectorHijo2 + "[" + x + "]/" + lastSon + "/text()";
            XPathFactory xpfac = XPathFactory.instance();
            XPathExpression<Content> expr = xpfac.compile(querie, Filters.content());
            List<Content> links = expr.evaluate(this.document);
            String outputXML = "";
            outputXML = new XMLOutputter().outputString(links);
            listaResultados.add(outputXML);
        }
        return listaResultados;
    }

}
