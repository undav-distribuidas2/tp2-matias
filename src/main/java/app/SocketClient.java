package app;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.io.ObjectOutputStream;

public class SocketClient {

    public final static int SOCKET_PORT = 13267;
    public final static String SERVER = "127.0.0.1";
    public final static int FILE_SIZE = 2048;   // file size temporary hard coded
    // should bigger than the file to be downloaded

    public void runClient(String fileName) throws IOException {
        int bytesRead;
        int current = 0;
        FileOutputStream fileOutputStream = null;
        BufferedOutputStream bufferedOutputStream = null;
        Socket socket = null;
        ObjectOutputStream objectOutputStream = null;

        try {
            socket = new Socket(SERVER, SOCKET_PORT);
            System.out.println("Connecting...");
            objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            objectOutputStream.writeObject(fileName);

            // receive file
            String FILE_TO_RECEIVED = "/home/matias/workspace-intellij/remoteXML/"+ fileName +".xml";
            byte[] mybytearray = new byte[FILE_SIZE];
            InputStream inputStream = socket.getInputStream();
            fileOutputStream = new FileOutputStream(FILE_TO_RECEIVED);
            bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
            bytesRead = inputStream.read(mybytearray, 0, mybytearray.length);
            current = bytesRead;
            do {
                bytesRead = inputStream.read(mybytearray, current, (mybytearray.length - current));
                if (bytesRead >= 0) current += bytesRead;
            } while (bytesRead > -1);

            bufferedOutputStream.write(mybytearray, 0, current);
            bufferedOutputStream.flush();
            System.out.println("File " + FILE_TO_RECEIVED + " downloaded (" + current + " bytes read)");
        } finally {
            if (fileOutputStream != null) fileOutputStream.close();
            if (bufferedOutputStream != null) bufferedOutputStream.close();
            if (socket != null) socket.close();
            objectOutputStream.close();
        }
    }
}
