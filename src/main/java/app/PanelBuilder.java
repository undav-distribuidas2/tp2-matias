package app;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class PanelBuilder extends JFrame implements ActionListener{

    List<String> listNameButtons;
    List<String> listXButtons;
    List<String> listYButtons;
    List<String> listWButtons;
    List<String> listHButtons;
    int sizeButtons;
    List<String> listXTextFields;
    List<String> listYTextFields;
    List<String> listWTextFields;
    List<String> listHTextFields;
    int sizeTextFields;


    public List<String> getListNameButtons() { return listNameButtons; }
    public void setListNameButtons(List<String> listNameButtons) { this.listNameButtons = listNameButtons; }
    public List<String> getListXButtons() { return listXButtons; }
    public void setListXButtons(List<String> listXButtons) { this.listXButtons = listXButtons; }
    public List<String> getListYButtons() { return listYButtons; }
    public void setListYButtons(List<String> listYButtons) { this.listYButtons = listYButtons; }
    public List<String> getListWButtons() { return listWButtons; }
    public void setListWButtons(List<String> listWButtons) { this.listWButtons = listWButtons; }
    public List<String> getListHButtons() { return listHButtons; }
    public void setListHButtons(List<String> listHButtons) { this.listHButtons = listHButtons; }
    public int getSizeButtons() { return sizeButtons; }
    public void setSizeButtons(int sizeButtons) { this.sizeButtons = sizeButtons; }

    public List<String> getListXTextFields() { return listXTextFields; }
    public void setListXTextFields(List<String> listXTextFields) { this.listXTextFields = listXTextFields; }
    public List<String> getListYTextFields() { return listYTextFields; }
    public void setListYTextFields(List<String> listYTextFields) { this.listYTextFields = listYTextFields; }
    public List<String> getListWTextFields() { return listWTextFields; }
    public void setListWTextFields(List<String> listWTextFields) { this.listWTextFields = listWTextFields; }
    public List<String> getListHTextFields() { return listHTextFields; }
    public void setListHTextFields(List<String> listHTextFields) { this.listHTextFields = listHTextFields; }
    public int getSizeTextFields() { return sizeTextFields; }
    public void setSizeTextFields(int sizeTextFields) { this.sizeTextFields = sizeTextFields; }

    public PanelBuilder(){
    }

    private void createButtons(JPanel panel){
        for(int aux=0; aux<getSizeButtons(); aux++){
            JButton button = new JButton(getListNameButtons().get(aux));
            int x = Integer.parseInt(getListXButtons().get(aux));
            int y = Integer.parseInt(getListYButtons().get(aux));
            int w = Integer.parseInt(getListWButtons().get(aux));
            int h = Integer.parseInt(getListHButtons().get(aux));
            button.setBounds(x,y,w,h);
            panel.add(button);
        }
    }

    private void createTexFields(JPanel panel){
        for(int aux=0; aux<getSizeTextFields(); aux++){
            JTextField textField = new JTextField();
            int x = Integer.parseInt(getListXTextFields().get(aux));
            int y = Integer.parseInt(getListYTextFields().get(aux));
            int w = Integer.parseInt(getListWTextFields().get(aux));
            int h = Integer.parseInt(getListHTextFields().get(aux));
            textField.setBounds(x,y,w,h);
            panel.add(textField);
        }
    }


    public void createFrame(){

        JFrame eFrame = new JFrame("Nuevo Frame equisde");

        JPanel ePanel = new JPanel();
        ePanel.setBounds(0,0,800,600);
        ePanel.setLayout(null);

        createButtons(ePanel);
        createTexFields(ePanel);

        eFrame.getContentPane().add(ePanel);
        eFrame.setSize(800,600);
        eFrame.getContentPane().setBackground(Color.white);
        eFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        eFrame.setVisible(true);

    }


    @Override
    public void actionPerformed(ActionEvent actionEvent) {

    }

}
